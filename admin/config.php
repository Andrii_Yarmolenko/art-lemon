<?php
// HTTP
define('HTTP_SERVER', 'http://art-lemon/admin/');
define('HTTP_CATALOG', 'http://art-lemon/');

// HTTPS
define('HTTPS_SERVER', 'http://art-lemon/admin/');
define('HTTPS_CATALOG', 'http://art-lemon/');

// DIR
define('DIR_APPLICATION', 'C:/OpenServer/OSPanel/domains/art-lemon/admin/');
define('DIR_SYSTEM', 'C:/OpenServer/OSPanel/domains/art-lemon/system/');
define('DIR_IMAGE', 'C:/OpenServer/OSPanel/domains/art-lemon/image/');
define('DIR_LANGUAGE', 'C:/OpenServer/OSPanel/domains/art-lemon/admin/language/');
define('DIR_TEMPLATE', 'C:/OpenServer/OSPanel/domains/art-lemon/admin/view/template/');
define('DIR_CONFIG', 'C:/OpenServer/OSPanel/domains/art-lemon/system/config/');
define('DIR_CACHE', 'C:/OpenServer/OSPanel/domains/art-lemon/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/OpenServer/OSPanel/domains/art-lemon/system/storage/download/');
define('DIR_LOGS', 'C:/OpenServer/OSPanel/domains/art-lemon/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/OpenServer/OSPanel/domains/art-lemon/system/storage/modification/');
define('DIR_UPLOAD', 'C:/OpenServer/OSPanel/domains/art-lemon/system/storage/upload/');
define('DIR_CATALOG', 'C:/OpenServer/OSPanel/domains/art-lemon/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'oc_artlemon');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
